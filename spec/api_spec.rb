ENV['RACK_ENV'] = 'test'

require 'yaml'

TEST_CONFIG = YAML.load_file('config.yml')[ENV['RACK_ENV']]

require 'rspec'
require 'rack/test'
require 'webmock/rspec'

WebMock.disable_net_connect!(allow_localhost: true)

RSpec.configure do |conf|
  conf.include Rack::Test::Methods
end

require_relative '../api'

describe 'API' do
  include Rack::Test::Methods

  def app
    Sinatra::Application
  end

  def auth_get(path, params = {})
    get path, params.merge(key: TEST_CONFIG['API_KEY'])
  end

  def required_response(status)
    {
      status: status,
      body: File.open("#{File.dirname(__FILE__)}/fixtures/#{status}.json").read,
      headers: { 'Content-Type': 'application/json' }
    }
  end

  context 'with an existing address' do
    let(:address) { 'checkpoint charlie' }

    before do
      stub_request(:get, /eu1.locationiq.com/).to_return(required_response(200))
    end

    it 'returns the corresponding coordinates' do
      auth_get '/location', address: address
      expect(last_response).to be_ok

      json = JSON.parse(last_response.body)
      expect(json['latitude']).to eq('52.5075459')
      expect(json['longitude']).to eq('13.3903685')
      expect(json['formatted_coordinates']).to eq('52.5075459, 13.3903685')
    end
  end

  context 'when api key is invalid' do
    before do
      get '/location'
    end

    it 'prevents access' do
      expect(last_response.status).to eq(401)
      json = JSON.parse(last_response.body)
      expect(json['status']).to eq(401)
      expect(json['message']).to eq('Unauthorized')
    end
  end

  context 'when address parameter is not given' do
    before do
      auth_get '/location'
    end

    it 'sets status to 400' do
      expect(last_response.status).to eq(400)
    end

    it 'returns an error messages' do
      json = JSON.parse(last_response.body)
      expect(json['status']).to eq(400)
      expect(json['message']).to eq('Missing required parameter: address.')
    end
  end

  context 'when empty address parameter given' do
    let(:address) { '' }

    before do
      auth_get '/location', address: address
    end

    it 'sets status to 400' do
      expect(last_response.status).to eq(400)
    end

    it 'returns an error message' do
      json = JSON.parse(last_response.body)
      expect(json['status']).to eq(400)
      expect(json['message']).to eq('Missing required parameter: address.')
    end
  end

  context 'when address is unknown / cannot be geocoded' do
    let(:address) { 'gibberishmcgibberface' }

    before do
      stub_request(:get, /eu1.locationiq.com/).to_return(required_response(404))

      auth_get '/location', address: address
    end

    it 'sets status to 404' do
      expect(last_response.status).to eq(404)
    end

    it 'returns an error message' do
      json = JSON.parse(last_response.body)
      expect(json['status']).to eq(404)
      expect(json['message']).to eq("Unknown address: #{address}.")
    end
  end

  context 'when LocationIQ API point cannot be found' do
    let(:address) { 'checkpoint charlie' }

    before do
      stub_request(:get, /eu1.locationiq.com/).to_return(
        required_response(404).merge(headers: {}))

      auth_get '/location', address: address
    end

    it 'sets status to 500' do
      expect(last_response.status).to eq(500)
    end

    it 'returns an error message' do
      json = JSON.parse(last_response.body)
      expect(json['status']).to eq(500)
      expect(json['message']).to eq('Internal Server Error')
    end
  end

  context 'when unexpected error' do
    let(:address) { 'checkpoint charlie' }

    before do
      stub_request(:get, /eu1.locationiq.com/).to_return(
        required_response(200).merge(status: 201)
      )

      auth_get '/location', address: address
    end

    it 'sets status to 500' do
      expect(last_response.status).to eq(500)
    end

    it 'returns an error message' do
      json = JSON.parse(last_response.body)
      expect(json['status']).to eq(500)
      expect(json['message']).to eq('Internal Server Error')
    end
  end

  context 'when LocationIQ times out' do
    let(:address) { 'checkpoint charlie' }

    before do
      stub_request(:get, /eu1.locationiq.com/).to_timeout

      auth_get '/location', address: address
    end

    it 'sets status to 503' do
      expect(last_response.status).to eq(503)
    end

    it 'returns an error message' do
      json = JSON.parse(last_response.body)
      expect(json['status']).to eq(503)
      expect(json['message']).to eq('Service Unavailable')
    end
  end
end
