require 'json'
require 'net/http'
require 'sinatra'
require 'yaml'


raise 'Forgot API_TOKEN?' unless ENV['API_TOKEN']
ENV['RACK_ENV'] ||= 'development'


API_POINT = "https://eu1.locationiq.com/v1/search.php?format=json&key=#{ENV['API_TOKEN']}"

# Normally store this in database, e.g. api_keys table and check against that
CONFIG = YAML.load_file('config.yml')[ENV['RACK_ENV']]
CONFIG.merge!(YAML.load_file('config.secrets.yml')[ENV['RACK_ENV']] || {})
API_KEY = CONFIG['API_KEY']


get '/location' do
  content_type :json

  # Prevent public access
  render_401 unless params['key'] == API_KEY

  # Address should be .present?
  address = params['address']
  render_400 if address.nil? || address.empty?

  # `open` from `open-uri` does not include the body if the status was 404.
  # But LocationIQ might return a reason, if the status is 404, e.g. "Unable to geocode".
  # A 404 because the route was unknown, e.g. /v1/notrightsearchurl.php, would be indistinguishable.
  # Therefore, use net/http instead.
  uri = URI.parse(URI.escape("#{API_POINT}&q=#{address}"))
  http = Net::HTTP.new(uri.host)

  begin
    response = http.get(uri.request_uri)
  rescue Net::OpenTimeout => _
    render_503
  end

  # URL changed / wrong URL, e.g. /v1/notrightsearchurl.php, or otherwise unexpected response
  # LocationIQ returns "File not found"
  render_500 unless response.content_type == 'application/json'

  json = JSON.parse(response.body)

  # If the address is unknown, LocationIQ returns 404 with error: Unable to geocode
  render_404(address) if response.code.to_i == 404 && json['error'] == 'Unable to geocode'

  # Any other unexpected error, e.g. api token invalid
  render_500 unless response.code.to_i == 200

  render_geocoded_address(address, json)
end

def render_geocoded_address(address, json_response)
  location = json_response[0]
  latitude = location['lat']
  longitude = location['lon']

  {
    address: address,
    latitude: latitude,
    longitude: longitude,
    formatted_coordinates: "#{latitude}, #{longitude}"
  }.to_json
end

def render_400
  halt 400, {
    status: 400,
    message: 'Missing required parameter: address.'
  }.to_json
end

def render_401
  halt 401, {
    status: 401,
    message: 'Unauthorized'
  }.to_json
end

def render_404(address)
  halt 404, {
    status: 404,
    message: "Unknown address: #{address}."
  }.to_json
end

def render_500
  halt 500, {
    status: 500,
    message: 'Internal Server Error'
  }.to_json
end

def render_503
  halt 503, {
    status: 503,
    message: 'Service Unavailable'
  }.to_json
end
