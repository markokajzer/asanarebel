# How to run

```
bundle install
API_TOKEN=YOUR_API_TOKEN ruby api.rb
API_TOKEN=YOUR_API_TOKEN bundle exec rspec
```

# Example Request

http://localhost:4567/location?address=checkpoint%20charlie&key=ABCDEFGHIJKLMNOPQRSTUVWXYZ

# Guideline Questions

+ How do you handle configuration values? What if those values change?
    + API_TOKEN for LocationIQ is provided via the command line. If the token changes, the app has to be restarted with the new token.
    + API_KEY for our API endpoint is provided via the config.yml (or config.secrets.yml in production). Normally it would be better to create a database table api_keys, create / store any API keys there, and check against the table when making requests to the API.
        + In our case if the key changes, the app has to be restarted. With a table, a new record could simply be created.
+ What happens if we encounter an error with the third-party API integration? Will it also
break our application, or are they handled accordingly?
    + Timeouts are handled with 503s.
    + If LocationIQ cannot find coordinates it returns a 404 with the error "Unable to geocode", which we give as well.
    + If there is a real 404, as in the API point is missing, we return a 500 since we (as in the users of such a geocoding API) should guarantee that the service works and should correct the URL.
    + If there is any other unexpected errors, e.g. invalid LocationIQ token, we handle them with 500s.

# Possible Optimizations

+ Requests and results could be stored in e.g. a hash to circumvent requesting LocationIQ.
